#include <Ethernet.h>
#include <SD.h>
#include <SPI.h>
#define REQ_BUFF_SIZE 40

//ethernet
byte mac[] = {0x90, 0xA2, 0xDA, 0x0F, 0xA6, 0xBD};
IPAddress ip(10, 0, 0, 2);
EthernetServer server(80);

File webfile;

char HTTP_req[REQ_BUFF_SIZE] = { 0 };
char req_index = 0;

char BLANK_LINE = '/n';

const int LM35 = A0; // Define o pino que lera a saída do LM35
float temperatura; // Variável que armazenará a temperatura medida
int ledPin = 7; //Led no pino 7
int ldrPin = 1; //LDR no pino analógico 1
int luz = 0; //Valor lido do LDR

void setup() {
  Serial.begin(9600); // inicializa a comunicação serial
  Ethernet.begin(mac, ip);
  Serial.println("Iniciando conexao..");
  server.begin();
  if (!SD.begin(4)) {
    Serial.println("ERRO - SD falhou");
  }
  Serial.println("SUCESSO - SD inicializado");

  if (!SD.exists("index.htm")) {
    Serial.println("ERRO - index.htm nao foi encontrado");
  }
  Serial.println("SUCESSO - index.htm encontrado");

  pinMode(ledPin, OUTPUT); //define a porta 7 como saída
}

void loop() {
  EthernetClient client = server.available();

  if (client) {
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        if (req_index < (REQ_BUFF_SIZE - 1)) {
          HTTP_req[req_index] = c;
          req_index++;
        }

        if (c == BLANK_LINE && currentLineIsBlank) {
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");
          client.println();

          if (StrContains(HTTP_req, "ajax_lerdados")) {
            lerDados(client);
          }
          else {
            webfile = SD.open("index.htm");
            if (webfile) {
              while (webfile.available()) {
                client.write(webfile.read());
              }
              webfile.close();
            }
          }
          Serial.println(HTTP_req);
          req_index = 0;
          StrClear(HTTP_req, REQ_BUFF_SIZE);
          break;
        }

        if (c == BLANK_LINE) {
          currentLineIsBlank = true;
        }
        else if (c != '\r') {
          currentLineIsBlank = false;
        }
      }
    }
    delay(1);
    client.stop();
  }
}

void lerDados(EthernetClient novoCliente) {
  luz = analogRead(ldrPin);
  luz = map(luz, 0, 1023, 0, 100);
  luz = (luz - 100)*-1;
  novoCliente.print(luz);
  novoCliente.println("%");
  novoCliente.print("|");
  temperatura = (float(analogRead(LM35)) * 5 / (1023)) / 0.01;
  novoCliente.print(temperatura);
  novoCliente.println("*C");
}

void StrClear(char *str, char length){
  for(int i = 0; i < length; i++){
    str[i] = 0;
  }
}

boolean StrContains(char *str, char *sfind) {
  char found = 0;
  char index = 0;
  char len;

  len = strlen(str);

  if (strlen(sfind) > len) {
    return false;
  }

  while (index < len) {
    if (str[index] == sfind[found]) {
      found++;
      if (strlen(sfind) == found) {
        return true;
      }
    }
    else {
      found = 0;
    }
    index++;
  }

  return false;
}

